﻿using System;
using System.Linq;

namespace FoxTales.Infrastructure.Extensions.Objects
{
    public static class ObjectExtensions
    {
        public static TResult SafeGetter<T, TResult>(this T me, Func<T, TResult> getter, TResult def = default(TResult)) where T : class
        {
            return me == null ? def : getter(me);
        }

        public static IQueryable<T> ToQueryable<T>(this T me)
        {
            return new[] { me }.AsQueryable();
        }
    }
}
