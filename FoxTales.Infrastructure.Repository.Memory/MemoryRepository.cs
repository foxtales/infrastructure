﻿// Copyright (C) 2014 FoxTales
// Released under the MIT License

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using FoxTales.Infrastructure.DomainFramework.Generics;
using FoxTales.Infrastructure.OperationFramework;
using FoxTales.Infrastructure.RepositoryFramework;
using FoxTales.Infrastructure.RepositoryFramework.Interfaces;
using FoxTales.Infrastructure.SpecificationFramework.Interfaces;
using FoxTales.Infrastructure.UnitOfWorkFramework.Interfaces;
using JetBrains.Annotations;

namespace FoxTales.Infrastructure.Repository.Memory
{
    public class MemoryRepository<T, TIdentity> : RepositoryBase<T, TIdentity> where T : EntityBase<TIdentity> where TIdentity : struct
    {
        private readonly IDictionary<TIdentity, T> _entities = new Dictionary<TIdentity, T>();

        public MemoryRepository([NotNull] IUnitOfWork<TIdentity> unitOfWork, [CanBeNull] IPersistenceSpecification<T> persistenceSpecification = null)
            : base(unitOfWork, persistenceSpecification)
        {
        }

        public override T FindById(TIdentity id)
        {
            return _entities.ContainsKey(id) ? _entities[id] : null;
        }

        public override IQueryable<T> FindByIds(params TIdentity[] ids)
        {
            var result = (from id in ids where _entities.ContainsKey(id) select _entities[id]).ToList();
            return result.AsQueryable();
        }

        public override OperationResult<RepositoryErrorCode> Update(T item, params Expression<Func<T, object>>[] properties)
        {
            return Update(item);
        }

        public override IQueryable<T> Query(ISpecification<T> specification = null, SoftDeleteQuerySetting softDeleteQuerySetting = SoftDeleteQuerySetting.Exclude)
        {
            var result = _entities.Values.AsQueryable();
            if (specification != null) result = result.Where(specification.IsSatisfied());

            switch (softDeleteQuerySetting)
            {
                case SoftDeleteQuerySetting.Exclude:
                    result = result.Where(i => i.SoftDeletedDate == null);
                    break;
                case SoftDeleteQuerySetting.DeletedOnly:
                    result = result.Where(i => i.SoftDeletedDate != null);
                    break;
            }

            return result;
        }

        public override IQueryable<T> QueryNew(ISpecification<T> specification = null, SoftDeleteQuerySetting softDeleteQuerySetting = SoftDeleteQuerySetting.Exclude)
        {
            return new List<T>().AsQueryable();
        }

        protected override void PersistNewItem(T item)
        {
            _entities.Add(item.Id, item);
        }

        protected override void PersistUpdatedItem(T item)
        {
        }

        protected override void PersistDeletedItem(T item)
        {
            _entities.Remove(item.Id);
        }
    }
}
