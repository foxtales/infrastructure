﻿// Copyright (C) 2014 FoxTales
// Released under the MIT License
 
namespace FoxTales.Infrastructure.SpecificationFramework.Interfaces
{
    public interface IDefaultQuerySpecification<T> : ISpecification<T>
    {
    }
}
